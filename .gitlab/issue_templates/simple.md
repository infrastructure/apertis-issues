Background
==========
> Explain your issue
> Steps to reproduce
> Expected result
> Actual result

Reproducibility
===============
How often the issue is hit when repeating the steps to reproduce and changing nothing?

Put the ✅ in the most appropriate entry:

1. always
2. ✅ often, but not always
3. rarely

Impact of bug
=============
> How severe is the bug? Does it render an image unbootable? Is it a security issue? Does it prevent specific applications from working?
> What is the impact? Does this bug affect a critical component? Does it cause something else to not work?
> How often is the bug likely to be found by a user? For example, every boot or once per year?

Outcomes
========
TBD

Management data
===============
This section is for management only, it should be the last one in the description.

/label ~"status:unconfirmed" ~"priority:needs triage" ~"release:v2025pre" ~"release:v2024" ~"release:v2023"

/cc @em @balasubramanian @sudarshan @wlozano
