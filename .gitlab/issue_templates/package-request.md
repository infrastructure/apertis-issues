> Replace the information below with the details about the package you're requesting and drop this line

What is the name of the component being requested?
==================================================
Podman

What are the minimum and maximum version required?
==================================================
Minimum: 3.0
Maximum: any

On which Apertis release channel the package should be made available?
======================================================================
v2024

Where the package can be sourced from?
======================================
**Put the "✅" in the appropriate entry and add the relevant details**

1. The required version is already in Apertis, but only on newer releases so it would need to be backported
2. ✅ The required version is already in the appropriate Debian baseline (Bullseye up to v2023, Bookworm since v2024)? (Use [packages.debian.org](https://packages.debian.org/) to find out)
    1. Debian link: https://packages.debian.org/source/bullseye/libpod
    2. Version: 3.0.1+dfsg1-1
3. The required version is only in newer versions of Debian than the appropriate Debian baseline
    1. Debian link: 
    2. Version: 
4. The package is public but not in Debian at all yet
    1. Upstream link to source repository:
5. The package is not public
    1. It is meant to become public?
    2. What is the Open Source license to be used? (Apertis [defaults to MPL-2.0](https://www.apertis.org/policies/license-applying/))
    3. Instructions to retrieve the sources, including how to obtain the required credentials:
    4. Instructions to build the component:
    5. Instructions to test the component:

Who needs it?
=============
The Apertis IoT team

What is the need that the package is meant to address?
======================================================
Running containerized workloads on edge devices, deployed using OCI images.

Is the package suitable for inclusion in the main Apertis archive?
==================================================================
Does the package bring any risk should it be made available to every Apertis users? For instance, backports can easily introduce regressions.

Is its usefulness general enough to be worth the additional long-term maintenance cost, or a one-off project-specific import is sufficient?

Is it more suitable for project-specific development areas rather than being shipped in the main Apertis archive?

What are the licenses that regulate the package usage and redistribution?
=========================================================================
**If in doubt, look at `COPYRIGHT` or `LICENSE` files in the sources to be packaged.**

Apache-2.0, see https://github.com/containers/podman/blob/master/LICENSE

Is the package meant to be used on deployed devices, or is only for development/debugging purposes?
===================================================================================================

**Put the "✅" in the appropriate entry**

1. The component is meant to be installed on deployed production devices where they need to meet strict [license expectations](https://www.apertis.org/policies/license-expectations/) (for instance, no GPL-3 code)
2. ✅ The component is meant to be installed on devices during development or production settings where generic OSS licensing compliant is sufficient
3. The component is not meant to be installed on devices but it is only used during development from the SDK

If not in Debian, who should be responsible for long-term maintenance
=====================================================================
Ensuring that packages are kept up-to-date compared to Debian is part of the Apertis #baseline effort.

But if in the "Where the package can be sourced from?" question any option other than 1 or 2 has been selected additional effort will be needed to ensure that the package is kept up-to-date compared to upstream, that the packaging metadata follow new best-practices appropriately and so on.

Management data
===============
This section is for management only, it should be the last one in the description.

/label ~"status:unconfirmed" ~"area:package-request" ~"priority:needs triage" ~"release:v2025pre"

/cc @em @balasubramanian @sudarshan @wlozano
