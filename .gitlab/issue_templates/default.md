Affected images versions
========================
* not relevant (explain why)
* see the table below (list the ***architecture** and **build id** of the tested images in the appropriate cells)

| Deployment  | Type                    | v2023     | v2024     | v2025pre  |
| ----        | ----                    | -----     | -----     | --------- |
| apt         | minimal/fixedfunction   |           |           |           |
| ostree      | minimal/fixedfunction   |           |           |           |
| apt         | target/hmi              |           |           |           |
| ostree      | target/hmi              |           |           |           |
| apt         | basesdk                 |           |           |           |
| apt         | sdk                     |           |           |           |
| apt         | nfs                     |           |           |           |
| apt         | lxc                     |           |           |           |
| apt         | image-builder           |           |           |           |
| apt         | package-source-builder  |           |           |           |

To find the build id and the variant type you can:
* derive it from the image name
  * for instance, with the `apertis_ostree_v2022pre-fixedfunction-amd64-uefi_20211031.0425.img.gz` image the build id is 20211031.0425, the variant is `fixedfunction` the deployment type is `ostree`
* obtain it from `/etc/os-release` using the `BUILD_ID` and `VARIANT_ID` keys

Unaffected images versions
==========================
* all versions up to XXX (replace XXX with the build id of the most recent Apertis images where this bug **cannot** be reproduced)   
* not relevant (explain why)
* v2022 (explain why)

Testcase
========
> The link to the testcase on https://qa.apertis.org/ if the bug was found during a testing round

Steps to reproduce
==================
> Ordered list detailing every step so other developers can reproduce the issue. Include hardware which you used.
* ...

Expected result
===============
> Explain (at least briefly) what result you expected.

Actual result
=============
> Explain the actual result observed after the execution steps.
> Paste any error output here between code block back-quotes.
> For long text contents (over 1000 lines) it is better to attach a file by using the button in the right corner.
If adding comments on the log is required [create a new snippet](https://gitlab.apertis.org/-/snippets/new) and add the link to it here.

Reproducibility
===============
How often the issue is hit when repeating the test and changing nothing (same device, same image, etc.)?

Put the ✅ in the most appropriate entry:

1. always
2. ✅ often, but not always
3. rarely

Impact of bug
=============
> How severe is the bug? Does it render an image unbootable? Is it a security issue? Does it prevent specific applications from working?
> What is the impact? Does this bug affect a critical component? Does it cause something else to not work?
> How often is the bug likely to be found by a user? For example, every boot or once per year?

Attachments
===========
> Add further information about the environment in the form of attachments here.
> Attach plain text files from log output (from `journalctl`, `systemctl`, …) or long backtraces as attached files.
If adding comments on the log is required [create a new snippet](https://gitlab.apertis.org/-/snippets/new) and add the link to it here.

> Screenshots and videos are usually useful for graphic issues.

Root cause
==========
> describe in one line what caused the issue to give a hint to product teams whether they may be impacted or not

Outcomes
========
TBD

Management data
===============
This section is for management only, it should be the last one in the description.

/label ~"status:unconfirmed" ~"priority:needs triage" ~"release:v2025pre" ~"release:v2024" ~"release:v2023"

/cc @em @balasubramanian @sudarshan @wlozano
