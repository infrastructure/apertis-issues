# Apertis issues

This repository hold the issue tracking for the Apertis project. Community members are encouraged to contribute
by reporting any issues they find while researching/developing/working with Apertis.

Under the [**Issues** section](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues) you will be able to check the current list and also to report your request for bug fixing or improvement.
The QA team will review new issues in order to apply the correct labels, including priority and areas of interest.

Besides the manual report, automated entries are created by the [QA Report App](https://gitlab.apertis.org/infrastructure/qa-report-app/)
as result of test failures in [LAVA](https://lava.collabora.co.uk/)

For more information please refer to the [Apertis website](https://www.apertis.org/).
